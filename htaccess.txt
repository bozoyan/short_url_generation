# nginx伪静态配置文件

location / {
    if (!-e $request_filename) {
        rewrite  ^([A-Za-z0-9]+)$  /info.php?c=$1  last;
        break;
    }
}

# apache伪静态配置文件

if (!-d $request_filename){
	set $rule_0 1$rule_0;
}
if (!-f $request_filename){
	set $rule_0 2$rule_0;
}
if ($rule_0 = "21"){
	rewrite ^/([A-Za-z0-9]+)$ /info.php?c=$1 last;
}