<?php

/**
 * redis 数据库
 * 短链接模式
 */
function redis()
{
    $host           = '127.0.0.1';      //主机地址
    $port           = 6379;             //端口号
    $timeout        = 10;               //超时时间
    $reserved       = null;             //保留
    $retry_interval = 0;                //重试间隔（毫秒）
    $password       = '';               //密码

    //连接服务
    $redis = new Redis();
    $redis->connect($host, $port, $timeout, $reserved, $retry_interval);
    $redis->auth($password);
    return $redis;
}
