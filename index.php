<?php require_once './config.php'; ?>
<!doctype html>
<html lang="zh-cn">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php echo $title; ?></title>
    <meta name="keywords" content="<?php echo $keywords; ?>"/>
    <meta name="description" content="<?php echo $description; ?>"/>
    <meta name="author" content="<?php echo $author; ?>"/>
    <link rel="icon" href="favicon.ico" type="image/x-icon"/>
    <link href="./css/bootstrap.css" rel="stylesheet">
    <style>
        .jumbotron {
            margin-top: 15px;
        }

        footer {
            width: 100%;
            height: 45px;
            line-height: 45px;
            border-top: solid 1px #EEE;
            color: #999;
            background-color: #eee;
        }

        .well {
            margin-top: 20px;
        }

        p {
            margin-bottom: 0;
        }
        .btn{
            margin-left: 10px;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="jumbotron">
        <h1>短网址生成工具</h1>
        <p>本站提供短网址服务，专业的网址缩短服务，具有稳定、快速、安全的特点。</p>
    </div>
    <div class="input-group">
        <input type="text" class="form-control" placeholder="请输入网址">
        <span class="input-group-btn">
            <button class="btn btn-default btn-success" type="button" id="create">生成</button>
        </span>
    </div>
    <div class="well well-sm hide">
        <p>短网址：<span id="res" data-clipboard-action="copy"></span><button class="btn btn-success" id="copyUrl">复制</button></p>
    </div>
    <div class="well">
        <p style="line-height: 2em;">站长QQ：297760026</p>
        <p style="line-height: 2em;">源码下载地址：<a href="http://d.shenlin.ink/amu367" rel="nofollow" target="_blank">
                http://d.shenlin.ink/amu367</a>
        </p>
        <p style="line-height: 2em;">
            搭建教程：
        </p>
        <p style="line-height: 2em;">
            1、需安装redis；
        </p>
        <p style="line-height: 2em;">
            2、设置伪静态，支持apache和nginx；
        </p>
        <p style="line-height: 2em;">
            3、生成短网址永久不过期；
        </p>
        <p style="line-height: 2em;">
            4、默认支持apache服务器；
        </p>
        <p style="line-height: 2em;">
            5、目录结构
        </p>
        <p style="line-height: 2em;">
            &nbsp;&nbsp;&nbsp;&nbsp;css 框架样式
        </p>
        <p style="line-height: 2em;">
            &nbsp;&nbsp;&nbsp;&nbsp;fonts 字体文件
        </p>
        <p style="line-height: 2em;">
            &nbsp;&nbsp;&nbsp;&nbsp;js 脚本文件
        </p>
        <p style="line-height: 2em;">
            &nbsp;&nbsp;&nbsp;&nbsp;.htaccess apache伪静态文件
        </p>
        <p style="line-height: 2em;">
            &nbsp;&nbsp;&nbsp;&nbsp;api.php 生成链接文件
        </p>
        <p style="line-height: 2em;">
            &nbsp;&nbsp;&nbsp;&nbsp;favicon.ico ico图标文件
        </p>
        <p style="line-height: 2em;">
            &nbsp;&nbsp;&nbsp;&nbsp;htaccess.txt 伪静态设置文件
        </p>
        <p style="line-height: 2em;">
            &nbsp;&nbsp;&nbsp;&nbsp;index.php 生成链接页面
        </p>
        <p style="line-height: 2em;">
            &nbsp;&nbsp;&nbsp;&nbsp;info.php 短链接访问文件
        </p>
        <p style="line-height: 2em;">
            &nbsp;&nbsp;&nbsp;&nbsp;redis.php redis链接文件
        </p>
        <p style="line-height: 2em;">
            6、api.php文件需要在第十行设置根目录；
        </p>
    </div>
</div>
<footer>
    <p class="text-center">版权所有：申霖</p>
</footer>
<script src="https://cdn.bootcss.com/jquery/3.4.1/jquery.min.js"></script>
<script src="./js/bootstrap.js"></script>
<script src="./js/clipboard.min.js"></script>
<script src="./layer/layer.js"></script>
<script>
    //复制链接
    var clipboard = new ClipboardJS('#copyUrl', {
        text: function () {
            return $('#res').html();
        }
    });
    clipboard.on('success', function (e) {
        layer.msg("复制成功");
        e.clearSelection();
        console.log(e.clearSelection);
    });
    clipboard.on('error', function (e) {
        layer.msg("当前浏览器不支持此功能，请手动复制。")
    });
    $("#create").click(function () {
        var url = $("input").val();
        if (!url || url.length < 1) {
            layer.msg("请输入网址");
        } else {
            $.ajax({
                type: 'post',
                url: './api.php',
                data: {url: url},
                async: false,
                dataType: 'json',
                success: function (res) {
                    if (res.code === 200) {
                        $(".well").removeClass("hide");
                        $("#res").html(res.data);
                    } else {
                        layer.msg(res.msg);
                    }
                },
                error: function () {
                    layer.msg("接口错误");
                }
            })
        }
        return false;
    });
</script>
</body>
</html>