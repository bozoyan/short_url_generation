<?php

$code = $_GET['c'];
if(!$code) {
    //链接
    echo '无法获取连接';
    die;
} else {
    require_once './config.php';
    require_once './redis.php';
    //数据库查找
    $url = redis()->get($code);
    if(!$url) {
        echo 'Unable to get connection';
        die;
    }
    //发出301头部
    header('HTTP/1.1 301 Moved Permanently');
    //跳转到你希望的地址格式
    header('Location: ' . $url);
    exit;
}
